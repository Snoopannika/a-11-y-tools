---
# try also 'default' to start simple
theme: seriph
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://images.unsplash.com/photo-1600202751116-3e0226e1e7a0
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
# persist drawings in exports and build
drawings:
  persist: false
# use UnoCSS (experimental)
css: unocss

fonts:
  sans: 'IBM Plex Sans'
  serif: 'IBM Plex Sans'
  mono: 'IBM Plex Mono'

---

<div class="border border-rounded-lg bg-white m-auto w-96">

# Accessibility

</div>

<div class="mt-16">

## What Developers can do

</div>



<div class="pt-12">
  <span @click="$slidev.nav.next" class="px-2 py-1 rounded cursor-pointer" hover="bg-white bg-opacity-10">
    Press Space for next page <carbon:arrow-right class="inline"/>
  </span>
</div>



<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---

# Accessibility for any Web-Application

<div class="mt-24"></div>

variety of tools we can use

- [axe](https://www.deque.com/axe/)
- [wave](https://wave.webaim.org/)
- [html-validate](https://html-validate.org/)
- [pa11y](https://pa11y.org/)
- [lighthouse](https://web.dev/lighthouse-accessibility/)

<div class="w-96 m-auto">
<figure>
    <img src="/accessibility-cartoon-by-Satu-Kyrolainen-700x273.png"
         alt="a11y comic">
    <figcaption class="text-xs text-right">image from <a href="http://www.satukyrolainen.com/right-thing-improve-accessibility/">satukyrolainen.com</a></figcaption>
</figure>
</div>

---

# let's test

<div class="mt-24"></div>

Test setup:

- [html 5 boilerplate](https://html5boilerplate.com/)
- adding 2 errors: 
  - img without alt tag
  - h5 Heading without parent Headings

---

# axe

<div class="mt-24"></div>

## what
- de-facto 'Standard' a11y Testing-Tool
- nodejs [CLI-Tool](https://github.com/dequelabs/axe-core) and [Browser Extension](https://www.deque.com/axe/)
- validates against WCAG
- well maintained

## how
- CI-Pipeline possible with CLI-Tool (some effort needed)
- whilst development with Browser Extension (or custom-made functionality)
- on productive Websites

---

# axe - Browser Extension

<div class="flex flex-row">
<div>
```html
  <!-- html5 boilerplate --> 

  <body>
    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```
</div>
<v-clicks at="1">
<div class="m-auto text-green-800">
  axe Browser Extension found our 2 Errors
</div>
</v-clicks>
</div>

<div class="m-auto w-2/3">
  <img src="/axe-browser_extension-cutted.png" alt="Axe Browser Extension" >
</div>

---

# axe - cli

<div class="flex flex-row gap-4 h-full">
<div class="flex flex-col h-full">
<div class="flex-1">
```html
  <!-- html5 boilerplate --> 

  <body>
    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```
</div>
<div class="flex-1 self-center">
<v-clicks at="1">
<div class="text-green-800">
  axe cli found our 2 Errors
</div>
</v-clicks>
</div>
</div>

<div class="text-xs w-2/3">
```bash {all|12-25,36-48}
[
  {
    id: 'color-contrast',
    impact: 'serious',
    tags: [ 'cat.color', 'wcag2aa', 'wcag143' ],
    description: 'Ensures the contrast between foreground and background colors meets WCAG 2 AA contrast ratio thresholds',
    help: 'Elements must have sufficient color contrast',
    helpUrl: 'https://dequeuniversity.com/rules/axe/4.4/color-contrast?application=axe-puppeteer',
    nodes: [ [Object], [Object], [Object], [Object] ]
  },
  {
    id: 'image-alt',
    impact: 'critical',
    tags: [
      'cat.text-alternatives',
      'wcag2a',
      'wcag111',
      'section508',
      'section508.22.a',
      'ACT'
    ],
    description: 'Ensures <img> elements have alternate text or a role of none or presentation',
    help: 'Images must have alternate text',
    helpUrl: 'https://dequeuniversity.com/rules/axe/4.4/image-alt?application=axe-puppeteer',
    nodes: [ [Object] ]
  },
  {
    id: 'landmark-one-main',
    impact: 'moderate',
    tags: [ 'cat.semantics', 'best-practice' ],
    description: 'Ensures the document has a main landmark',
    help: 'Document should have one main landmark',
    helpUrl: 'https://dequeuniversity.com/rules/axe/4.4/landmark-one-main?application=axe-puppeteer',
    nodes: [ [Object] ]
  },
  {
    id: 'region',
    impact: 'moderate',
    tags: [ 'cat.keyboard', 'best-practice' ],
    description: 'Ensures all page content is contained by landmarks',
    help: 'All page content should be contained by landmarks',
    helpUrl: 'https://dequeuniversity.com/rules/axe/4.4/region?application=axe-puppeteer',
    nodes: [
      [Object], [Object],
      [Object], [Object],
      [Object], [Object],
      [Object]
    ]
  }
]
```
</div>
</div>

---

# wave

<div class="mt-24"></div>

## what
- [Browser Extension](https://wave.webaim.org/extension/)
- validates against WCAG
- validates HTML
- maintenance Status unclear

## how
- whilst development with Browser Extension
- on productive Websites

--- 

# wave

<div class="flex flex-row gap-4 h-full">
<div class="flex flex-col h-full">
<div class="flex-1">
```html
  <!-- html5 boilerplate --> 

  <body>
    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```
</div>
<div class="flex-1">
<v-clicks at="1">
<div class="m-auto text-green-800">
  wave Browser Extension found our 2 Errors
</div>
</v-clicks></div>

</div>
<div class="m-auto w-60">
  <img src="/wave-cutted.png" alt="Wave Browser Extension" >
</div>
</div>

---

# html-validate

<div class="mt-24"></div>

## what
- nodejs [CLI-Tool](https://html-validate.org/usage/index.html)
- validates HTML
- validates some Specs from WCAG 2.1
- good maintained

## how
- CI-Pipeline possible
- can be used during Development with an extra Task

---

# html-validate

<div class="flex flex-row gap-4 h-full">
<div class="flex flex-col h-full">
<div class="flex-1">
```html
  <!-- html5 boilerplate --> 

  <body>
    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```
</div>
<div class="flex-1 self-center">
<v-clicks at="1">
<div class="m-auto text-red-800">
  html-validate missed 1 Error
</div>
</v-clicks>
</div>
</div>
<div class="text-xs">
```bash {all|7}
> a11y-tools@0.0.1 lint:html /home/dkuehn/tmp/a11y-tools
> html-validate --formatter text -c .htmlvalidate.json --ext html /tmp/boilerplate

/tmp/boilerplate/404.html:1:1: error [doctype-style] DOCTYPE should be uppercase
/tmp/boilerplate/index.html:1:1: error [doctype-style] DOCTYPE should be uppercase
/tmp/boilerplate/index.html:6:4: error [empty-title] <title> cannot be empty, must have text content
/tmp/boilerplate/index.html:28:4: error [wcag/h37] <img> is missing required "alt" attribute
 ELIFECYCLE  Command failed with exit code 1.
```
</div>
</div>

---

# pa11y

<div class="mt-24"></div>

## what

- nodejs [CLI-Tool](https://github.com/pa11y/pa11y)
- can validate against WCAG (must set in options)
- validates HTML
- somewhat maintained

## how 

- CI-Pipeline possible with an extra Task
- whilst development with an extra Task
- on productive Websites

---

# pa11y

<div class="flex flex-row gap-4 h-full">
<div class="flex flex-col h-full">
<div class="flex-1">
```html
  <!-- html5 boilerplate --> 

  <body>
    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```
</div>
<div class="flex-1 self-center">
<v-clicks at="1">
<div class="m-auto text-red-800">
  pa11y missed 1 Error
</div>
</v-clicks>
</div>
</div>
<div class="text-xs">
```bash {all|20-26}
pa11y-ci http://localhost:3000
Running Pa11y on 1 URLs:
 > http://localhost:3000/ - 3 errors

Errors in http://localhost:3000/:

• The title element in the head section should be non-empty.

(html > head > title)

   <title></title>

• The language specified in the lang attribute of the document element does
not appear to be well-formed.

()

[no context]

• Img element missing an alt attribute. Use the alt attribute to specify a
short text alternative.

(html > body > img)

   <img src="./assets/logo.png">

✘ 0/1 URLs passed
```
</div>
</div>

---

# pa11y - with options axe

<div class="flex flex-row gap-4 h-full">
<div class="flex flex-col h-full">
<div class="flex-1">
```html
  <!-- html5 boilerplate --> 

  <body>
    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```
</div>
<div class="flex-1 self-center">
<v-clicks at="1">
<div class="m-auto text-red-800" >
  pa11y missed 1 Error
</div>
</v-clicks>
</div>
</div>
<div class="text-xs">
```bash {all|46-52,27-33}
pa11y-ci -c /home/dkuehn/tmp/a11y-tools/.pa11yci http://localhost:3000
Running Pa11y on 1 URLs:
 > http://localhost:3000/ - 6 errors

Errors in http://localhost:3000/:

 • Documents must have <title> element to aid in navigation
   (https://dequeuniversity.com/rules/axe/4.4/document-title?application=axeAPI)

   (html)

   <html class="js sizes customelements history pointerevents postmessage
   postmessage-structuredclones webgl websockets cssanimations csscolumns
   csscolumns-width csscolumns-span csscolumns-fill csscolumns-gap
   csscolumns-rule csscolumns-rulecolor csscolu...

 • <html> element must have a lang attribute
   (https://dequeuniversity.com/rules/axe/4.4/html-has-lang?application=axeAPI)

   (html)

   <html class="js sizes customelements history pointerevents postmessage
   postmessage-structuredclones webgl websockets cssanimations csscolumns
   csscolumns-width csscolumns-span csscolumns-fill csscolumns-gap
   csscolumns-rule csscolumns-rulecolor csscolu...

 • Images must have alternate text
   (https://dequeuniversity.com/rules/axe/4.4/image-alt?application=axeAPI)

   (html > body > img)

   <img src="./assets/logo.png">

 • The title element in the head section should be non-empty.

   (html > head > title)

   <title></title>

 • The language specified in the lang attribute of the document element does
   not appear to be well-formed.

   ()

   [no context]

 • Img element missing an alt attribute. Use the alt attribute to specify a
   short text alternative.

   (html > body > img)

   <img src="./assets/logo.png">

✘ 0/1 URLs passed
```
</div>
</div>

---

# lighthouse

<div class="mt-24"></div>

## what

- nodejs [CLI-Tool](https://github.com/GoogleChrome/lighthouse) and Chrome builtin (Developer Tools)
- validate against some WCAG
- good maintained

## how

- CI-Pipeline possible with CLI-Tool (some effort needed)
- whilst development with Chrome
- on productive Websites

---

# lighthouse - Chrome Browser builtin

<div class="flex flex-row">

<div>
```html
  <!-- html5 boilerplate --> 

  <body>
    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```

</div>

<v-clicks at="1">
<div class="m-auto text-red-800">
  lighthouse missed 1 Error
</div>
</v-clicks>

</div>

<div class="m-auto mt-16">
  <img src="/lighthouse-cutted.png" alt="lighthouse a11y scan" >
</div>

---

# Accessibility for Vue/Nuxt Applications

<div class="mt-24"></div>

Some tools for static code analysis

- [html-validate-vue](https://html-validate.org/frameworks/vue.html)
- [eslint-plugin-vuejs-accessibility](https://github.com/vue-a11y/eslint-plugin-vuejs-accessibility)

More at [awesome a11y vue](https://github.com/vue-a11y/awesome-a11y-vue), but watch out for
maintenance status of various plugins/tools

---

# let's test

<div class="mt-24"></div>

Test setup:

- default vue3 vite ts setup
- adding 2 errors in `App.vue`:
  - img without alt tag
  - h5 Heading without parent Headings

---

# html-validate-vue

<div class="mt-24"></div>

## what

- nodejs [CLI-Tool](https://html-validate.org/frameworks/vue.html)
- validates HTML
- validates some Specs from WCAG 2.1
- good maintained

## how

- CI-Pipeline possible with an extra Task
- whilst development with an extra Task


---

# html-validate-vue

<div class="flex flex-row gap-4 h-full">
<div class="flex flex-col h-full">
<div class="flex-1">
```vue
  <!-- default vue template code --> 

    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```
</div>
<div class="flex-1 self-center">
<v-clicks at="1">
<div class="m-auto text-red-800" >
  html-validate-vue missed 1 Error
</div>
</v-clicks>
</div>
</div>

<div class="text-xs">
```bash {all|6}
~/.nvm/versions/node/v18.2.0/bin/pnpm run lint:html

> a11y-tools@0.0.1 lint:html /home/dkuehn/tmp/a11y-tools
> html-validate --formatter text -c .htmlvalidate.json --ext html,vue src

~/tmp/a11y-tools/src/App.vue:21:4: error [wcag/h37] <img> is missing required "alt" attribute
 ELIFECYCLE  Command failed with exit code 1.

Process finished with exit code 1
```
</div>

</div>



---

# eslint-plugin-vuejs-accessibility

<div class="mt-24"></div>

## what

- eslint [plugin](https://github.com/vue-a11y/eslint-plugin-vuejs-accessibility)
- validates some a11y concerns (e.g. keyboard bindings)
- somewhat maintained

## how 

- CI-Pipeline possible via eslint
- whilst development via eslint

---

# eslint-plugin-vuejs-accessibility

<div class="flex flex-row">
<div>
```vue
  <!-- default vue template code --> 

    <!-- heading is semantically wrong here -->
    <h5>h5 heading</h5>
    <!-- img must have an alt tag -->
    <img src="./assets/logo.png">

  <!-- more Stuff -->
```
</div>

<v-clicks at="1">
<div class="m-auto text-red-800">
  eslint-plugin-vuejs-accessibility missed 1 Error
</div>
</v-clicks>
</div>

<div class="m-auto mt-24">
  <img src="/eslint-plugin-vuejs-accessibility.png" alt="eslint-plugin-vuejs-accessibility" >
</div>

--- 

# Conclusion

<div class="mt-24"></div>

- every single Developer should take care for accessibility
- every single Developer has the power to do an accessibilty audit
- currently no free Tool to rule them all 

---

<div class="mt-24"></div>

thx for your time

