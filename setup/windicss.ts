import { defineWindiSetup } from '@slidev/types';

// extending the builtin windicss configurations
export default defineWindiSetup(() => ({
  theme: {
    extend: {
      // fonts can be replaced here, remember to update the web font links in `index.html`
      fontFamily: {
        sans: '"IBM Plex Sans"',
        serif: '"IBM Plex Sans"',
        mono: '"IBM Plex Mono", monospace',
      },
    },
  },
}))
