# Knowledge Transfer about Accessibility Tools

All credits to Daniel Kühn!

## Slides

### To start the slide show:

- `npm install`
- `npm run dev`
- visit http://localhost:3030

### To build the slide show:

- `npm install`
- `npm run build`

run `npm install -g serve`
run `serve ./dist` from project root

Edit the [slides.md](./slides.md) to see the changes.

## Testing Setups for Accessibility Knowledge Transfer

To start the setups locally, run:
```
cd testSetups
npm install
npm run dev
```
### HTML 

- find and edit the html file at `./testSetups/testSetupHTML.html`
- visit [http://localhost:5173/testSetupHTML.html](http://localhost:5173/testSetupHTML.html)

### Vue.js 

- find and edit the start page at `./testSetups/src/components/HelloWorld.vue`
- visit [http://localhost:5173/](http://localhost:5173/) (as your CLI should tell you)
- for more information about `vue.js` with `vite`, visit [https://miyauchi.dev/posts/vite-vue3-typescript/](https://miyauchi.dev/posts/vite-vue3-typescript/)
